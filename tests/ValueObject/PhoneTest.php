<?php
/**
 * Created by PhpStorm.
 * User: komartvk
 * Date: 5/17/17
 * Time: 8:34 AM
 */

namespace MiamiOH\Common\ValueObject;
use PHPUnit\Framework\TestCase;


class PhoneTest extends TestCase
{
    private $phone;
    
    public function testCanBeCreateFromValidPhoneNumber()
    {
        $this->phone = new Phone('+19365777251');
        $this->assertInstanceOf(Phone::class,$this->phone);
    }
    public function testCanNotBeCreateFromValidPhoneNumber()
    {
        $this->expectException('libphonenumber\NumberParseException');
        $this->phone = new Phone('+19365777251sdsdffgdfgf');
    }
}