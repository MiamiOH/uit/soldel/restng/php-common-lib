<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 5/11/17
 * Time: 9:40 AM
 */

namespace MiamiOH\Common\ValueObject;


use PHPUnit\Framework\TestCase;
use MiamiOH\Common\Exception\InvalidEmailException;

class EmailTest extends TestCase
{
    public function testCanCreateFromValidString()
    {
        $this->assertInstanceOf(Email::class, new Email('user@miamioh.edu'));
    }

    public function testCanNotBeCreatedFromInvalid()
    {
        $this->expectException(InvalidEmailException::class);
        $email = new Email('invalid');
    }

    public function testBeAbleToGetAsString()
    {
        $email = new Email('user@miamioh.edu');
        $this->assertEquals('user@miamioh.edu', (string)$email);
    }
}