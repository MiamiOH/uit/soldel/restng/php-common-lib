<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 5/11/17
 * Time: 9:39 AM
 */

namespace MiamiOH\Common\ValueObject;

use MiamiOH\Common\Exception\InvalidEmailException;
class Email
{
    /**
     * @var string
     */
    private $email;

    public function __construct(string  $email)
    {
        $this->validateEmail($email);
        $this->email = $email;
    }

    private function validateEmail($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException();
        }
    }

    public function __toString() : string
    {
        return $this->email;
    }
}