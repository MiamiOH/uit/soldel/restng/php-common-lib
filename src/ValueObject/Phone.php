<?php
/**
 * Created by PhpStorm.
 * User: komartvk
 * Date: 5/17/17
 * Time: 8:33 AM
 */

namespace MiamiOH\Common\ValueObject;


class Phone
{
    private $phone;
    
    public function __construct(string  $phone)
    {
        $this->isPhoneNumber($phone);
        $this->phone = $phone;

    }

    private function isPhoneNumber(string $phone) : void
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $parsedPhoneNumber = $phoneUtil->parse($phone, null, null, true);
    }

    public function __toString() : string
    {
        return $this->phone;
    }
}